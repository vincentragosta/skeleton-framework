<?php
/*
This is a sample local-config.php file
In it, you *must* include the three main database defines

You may include other settings here for various environments
*/

define( 'DB_NAME', '{db_name}' );
define( 'DB_USER', '{db_user}' );
define( 'DB_PASSWORD', '{db_password}' );
