#!/usr/bin/env bash

while getopts "d" option
do
 case "${option}"
 in
 d)
   DRYRUN_OPTION=1
   ;;
 esac
done

DRYRUN=${DRYRUN_OPTION:-0}

basedir=$(pwd)
themesdir="$basedir/wp-content/themes"

read -p "Site Name: " SITE_NAME

DEFAULT_THEME_NAME=$(echo "$SITE_NAME" | tr ' ' - | tr '[:upper:]' '[:lower:]')
read -p "Theme directory name: [${DEFAULT_THEME_NAME}] " THEME
THEME=${THEME:-${DEFAULT_THEME_NAME}}

read -p "Git repo for ${SITE_NAME}: " GIT_REPO
if [[ GIT_REPO -eq "" ]]
then
    PACKAGE_NAME="vincentragosta/$THEME"
else
    PACKAGE_NAME="vincentragosta/$(php init-replace.php .git '' $(basename $GIT_REPO))"
fi

DEFAULT_LOCAL_URL=$(echo "http://$THEME.test" | tr -d '-')
read -p "Local URL [${DEFAULT_LOCAL_URL}]: " LOCAL_URL
LOCAL_URL=${LOCAL_URL:-${DEFAULT_LOCAL_URL}}

DEFAULT_DB_NAME="scotchbox"
read -p "Default local db name: [${DEFAULT_DB_NAME}] " DB_NAME
DB_NAME=${DB_NAME:-${DEFAULT_DB_NAME}}

read -p "Default local db user: [root] " DB_USER
DB_USER=${DB_USER:-root}

read -p "Default local db password: [root] " DB_PASSWORD
DB_PASSWORD=${DB_PASSWORD:-root}

php init-replace.php themes/orchestrator-child "themes/$THEME" vincentragosta/skeleton-framework $PACKAGE_NAME composer.json
php init-replace.php {db_name} $DB_NAME {db_user} $DB_USER {db_password} $DB_PASSWORD local-config-sample.php
php init-replace.php {theme-name} $THEME README.md

echo "Fetching WP salts"
curl -s 'https://api.wordpress.org/secret-key/1.1/salt' | php init-replace.php //{salts}// wp-config.php

cp "local-config-sample.php" "local-config.php"

if [[ DRYRUN -eq 1 ]]
then
    exit
fi

# Point of no return!

composer install

childthemedir="$themesdir/orchestrator-child"
themedir="$themesdir/$THEME"
php init-replace.php "Orchestrator Child" "$SITE_NAME" $childthemedir/style.css
php init-replace.php {local_url} $LOCAL_URL $childthemedir/assets/manifest.json

mv $childthemedir $themedir

composer remove vincentragosta/orchestrator-child --dev

rm -rf "$themedir/.git"
rm -rf "$themedir/composer.json"
rm -rf "./.git"
git init
git add -A .
git reset init.sh init-replace.php
git commit -am "Import skeleton"
if [[ ! GIT_REPO -eq "" ]]
then
    git remote add origin $GIT_REPO
    git push --set-upstream origin master
    git checkout -b develop
    git push --set-upstream origin develop
fi

rm -f "./init.sh" "./init-replace.php"
